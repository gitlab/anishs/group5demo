# Group5Project
Gameplay Video Link:
[https://www.dropbox.com/s/yy4vfd4fdcqpjdx/GamePlayVideo1.webm?dl=0](https://www.dropbox.com/s/yy4vfd4fdcqpjdx/GamePlayVideo1.webm?dl=0)

# Title
8-bit Invaders

# Category:
Animated Game

# Description:
We are building a Space Shooter-like game in an 8-bit style.

# Roles
Lead: AnishSinha [14.29%], KrishShah [14.29%], NathanStrahs [14.29%], JamesKnee [14.29%], ThomasPoimenidis [14.29%], DerekXu [14.29%], JiaweiXiang [14.29%]

Front: AnishSinha [14.29%], KrishShah [14.29%], NathanStrahs [14.29%], JamesKnee [14.29%], ThomasPoimenidis [14.29%], DerekXu [14.29%], JiaweiXiang [14.29%]

Back: AnishSinha [14.29%], KrishShah [14.29%], NathanStrahs [14.29%], JamesKnee [14.29%], ThomasPoimenidis [14.29%], DerekXu [14.29%], JiaweiXiang [14.29%]

Documenter: AnishSinha [14.29%], KrishShah [14.29%], NathanStrahs [14.29%], JamesKnee [14.29%], ThomasPoimenidis [14.29%], DerekXu [14.29%], JiaweiXiang [14.29%]

Tester: AnishSinha [14.29%], KrishShah [14.29%], NathanStrahs [14.29%], JamesKnee [14.29%], ThomasPoimenidis [14.29%], DerekXu [14.29%], JiaweiXiang [14.29%]

# Member Names:
Anish Sinha, Thomas Poimenidis, Jiawei Xiang(DavidXiang), Krish Shah, Nathan Strahs, Derek Xu, James Knee

# Personalize Gitlab Task:
Completed

# Instructions
## INSTALL INSTRUCTIONS

In order to run 8-Bit Invaders, you must first have Android Studio installed [[install](https://developer.android.com/studio)] on your device. 
After downloading, you are ready to create a new project in Android Studio [Do not open the repo for 8-Bit Invaders – you will need to download more software before opening the 8-Bit Invaders project code]. To create a new project, open android studio, create a new project. From templates choose Phone and Tablets -> No Activity -> next. Then pick a name (does not matter because it is temporary), choose java as a language, and then press finish.

After you finish creating the project, please download an Android Emulator [[install](https://developer.android.com/studio/run/emulator)]. 

In order to run our project you will also need to download Android NDK and CMake. To do this please follow these instructions [[install](https://developer.android.com/studio/projects/install-ndk)].

After installing these components, you will need to clone the project repository to your local device. To do this, please use your computer’s terminal and use the following instructions:

1. Make sure you have git installed on your computer [[install](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)]
2. Then create a directory [through your file explorer or terminal -- NOT IN ANDROID STUDIO] in which you would like to clone the project, and subsequently navigate to that directory in your terminal.
3. Then you will need to clone this repository. To do this use the command:

`git clone https://agile.bu.edu/gitlab/anishs/group5demo.git`

NOTE: The https link is the HTTPS link when you navigate to the project repo on Gitlab, and use the blue drop down on the right hand side of the screen [the link provided may be out of date upon your cloning]. 

After downloading all of the above, you are ready to open the 8-Bit Invaders code. To do this navigate to File->Open. Then find the directory in which you cloned the project, and open the project. You may need to wait a few minutes for the project to configure everything. You will then be ready to run the project.

## Backup Video Link

https://www.dropbox.com/s/4n9mrwet8zpj1wx/GamePlayVideo1.webm?dl=0



